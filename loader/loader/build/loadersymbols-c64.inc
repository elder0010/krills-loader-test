; zeropage
loadaddrlo      = $10
loader_zp_first = $10
loadaddrhi      = $11
decdestlo       = $12
decdesthi       = $13
loader_zp_last  = $1e

; resident
loadraw         = $2000
loadcompd       = $200d
loadedtab       = $234f
decrunch        = $236f

; install
install         = $8fa6
