# Krill's Loader Test

### Based on
- [Krill's Loader repository version 146]

### Provided in the repository
- 6502 Assembly Source code ([Kick Assembler] format)
- Executable binaries
- Compatible [CA65] suite for compiling the loader on Windows
- Windows make procedure

### Compiling

You'll need [Java] to compile. To check if you have it use this command:
```
java -version
```
To compile, simply use the included make procedure:
```
make.bat
```

### Building the Loader
See
- loader/loader/docs/Usage.txt
- loader/loader/include/config.inc

**Free Software, Hell Yeah!**

[Kick Assembler]:http://www.theweb.dk/KickAssembler/Main.php
[Java]:http://java.com/en/download/index.jsp
[CA65]:http://www.cc65.org
[Krill's Loader repository version 146]:http://csdb.dk/release/?id=118713
