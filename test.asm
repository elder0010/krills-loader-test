.import source "functions.asm"
.var music = LoadSid("data/music.sid")
.pc=music.location "Music" .fill music.size, music.getData(i)

:BasicUpstart2(code)

.var pic = $4000
.var screen_1 = $6800

.const krill_resident = $2000
.const krill_install = $8000

.const loadraw = $2000
.const loadcompd = $200d
.const loadedtab = $234f
.const decrunch = $236f
.const install = $8fa6

.const pic_selector = $40


.pc = $0900 "code"
code:
        jsr install

        sei

        lax #0
        jsr music.init

        :kernal_off()
        :turn_off_cia_turn_on_raster_irq()

        :set_irq(irq)
        :d012($f9)
        :d011($1b)

        lda #0
        sta pic_selector
        sta $d011

        cli

        lda #BLACK
        sta $d020
        sta $d021

next_pic:
        lda pic_selector
        eor #$ff
        sta pic_selector

        beq !+
        ldx #<filename_a
        ldy #>filename_a
        jmp loadnx
!:
        ldx #<filename_b
        ldy #>filename_b
loadnx:
        jsr loadcompd

        lda #$a0
        sta $d018

        lda #$d8
        sta $d016

        :bank_1()

        jsr draw_pic
        lda #$3b
        sta $d011

spacechk:
        lda $dc01
        cmp #$ef
        bne spacechk

        :wait_frame()

        lda #0
        sta $d011

        jmp next_pic

irq:
        :open_irq()

        asl $d019

        jsr music.play

        :close_irq()
        rti

draw_pic:

        ldx #0
!:
        lda pic+$1f40,x
        sta screen_1,x

        lda pic+$1f40+$100,x
        sta screen_1+$100,x

        lda pic+$1f40+$200,x
        sta screen_1+$200,x

        lda pic+$1f40+$300,x
        sta screen_1+$300,x

        lda pic+$2328,x
        sta $d800,x

        lda pic+$2328+$100,x
        sta $d800+$100,x

        lda pic+$2328+$200,x
        sta $d800+$200,x

        lda pic+$2328+$300,x
        sta $d800+$300,x

        dex
        bne !-

        rts

.align $20
filename_a:
.text "00"
.byte 0

filename_b:
.text "01"
.byte 0

.pc = krill_resident "krill's loader"
.import c64 "loader/loader/build/loader-c64.prg"

.pc = krill_install "krill's installer"
.import c64 "loader/loader/build/install-c64.prg"
